function getValues() {
    let longueur = document.getElementById('longueur').value;
    let largeur = document.getElementById('largeur').value;
    let hauteur = document.getElementById('hauteur').value;

    let result = {};
    result.longueur = longueur;
    result.largeur = largeur;
    result.hauteur = hauteur;

    calculer(result);
}

function calculer(infos) {
    let pente = Math.sqrt((Math.pow(infos.hauteur, 2) + (Math.pow(infos.largeur/2), 2)));
    let surface = Math.round(pente * infos.longueur) * 2;
    alert(surface);
}
